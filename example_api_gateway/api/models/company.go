package models

type CreateCompany struct {
	Name string `json:"name" binding:"required"`
}

type Company struct {
	ID   string `json:"id"`
	Name string `json:"name" binding:"required"`
}

type GetAllCompanyResponse struct {
	Companys []Company `json:"companies"`
	Count       int32             `json:"count"`
}
