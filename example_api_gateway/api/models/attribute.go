package models

type CreateAttributeModel struct {
	Name string `json:"name" binding:"required"`
	Type string `json:"attribute_type" binding:"required"`
}

type Attribute struct {
	ID   string `json:"id"`
	Name string `json:"name" binding:"required"`
	Type string `json:"attribute_type" binding:"required"`
}

type GetAllAttributeResponse  struct {
	Attributes []Attribute `json:"attributes"`
	Count      int64            `json:"count"`
}
