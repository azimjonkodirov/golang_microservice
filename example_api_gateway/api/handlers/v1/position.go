package v1

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"example_api_gateway/api/models"
	"example_api_gateway/genproto/position_service"
	"example_api_gateway/pkg/util"

	"github.com/gin-gonic/gin"
)

// Create Position godoc
// @ID create-position
// @Router /v1/position [POST]
// @Summary Create Position
// @Description Create Position
// @Tags position
// @Accept json
// @Produce json
// @Param position body models.CreatePositionModel true "position"
// @Success 200 {object} models.ResponseModel{data=string} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) CreatePosition(c *gin.Context) {
	var position models.CreatePositionModel
	var pos_attributes []*position_service.PositionAttribute
	err := c.BindJSON(&position)
	if err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}
	for _, val := range position.PositionAttributes{
		pos_attributes = append(pos_attributes, &position_service.PositionAttribute{
			AttributeId: val.AttributeId,
			Value: val.Value,
		})
	}
	resp, err := h.services.PositionService().Create(
		context.Background(),
		&position_service.CreatePositionRequest{
			Name:                 position.Name,
			ProfessionId:         position.ProfessionId,
			CompanyId:            position.CompanyId,
			PositionAttributes:   pos_attributes,
		},
	)

	if !handleError(h.log, c, err, "error while creating position") {
		return
	}
	fmt.Println(position)
	err = ParseToStruct(&position, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// Get All Position godoc
// @ID get-all-position
// @Router /v1/position [GET]
// @Summary Get All Position
// @Description Get All Position
// @Tags position
// @Accept json
// @Produce json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param name query string false "name"
// @Param profession_id query string false "profession_id"
// @Param company_id query string false "company_id"
// @Success 200 {object} models.ResponseModel{data=models.CreatePositionModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetAllPositions(c *gin.Context) {
	var status models.PositionModel
	offset, err := h.ParseQueryParam(c, "offset", "0")
	if err != nil {
		return
	}

	limit, err := h.ParseQueryParam(c, "limit", "10")
	if err != nil {
		return
	}

	resp, err := h.services.PositionService().GetAll(
		context.Background(),
		&position_service.GetAllPositionRequest{
			Offset:               int64(offset),
			Limit:                int64(limit),
			Name:                 c.Query("name"),
			ProfessionId:         c.Query("profession_id"),
			CompanyId:            c.Query("company_id"),
		},
	)
	if !handleError(h.log, c, err, "error while getting all positions") {
		return
	}
	
	err = ParseToStruct(&status, resp)
	if !handleError(h.log, c, err, "error while parse to struct") {
		return
	}
	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// Get Position godoc
// @ID get-position
// @Router /v1/position/{position_id} [GET]
// @Summary Get Position
// @Description Get Position
// @Tags position
// @Accept json
// @Produce json
// @Param position_id path string true "position_id"
// @Success 200 {object} models.ResponseModel{data=models.CreatePositionModel} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) GetPosition(c *gin.Context) {
	position_id := c.Param("position_id")

	if !util.IsValidUUID(position_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid position id", errors.New("position id is not valid"))
		return
	}

	
	resp, err := h.services.PositionService().Get(
		context.Background(),
		&position_service.PositionId{
			Id: position_id,
		},
		
	)

	if !handleError(h.log, c, err, "error while getting position") {
		return
	}
	
	err = ParseToStruct(resp, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", resp)
}

// Update Position godoc
// @ID update-position
// @Router /v1/position/{position_id} [PUT]
// @Summary Update Position
// @Description Update Position by ID
// @Tags position
// @Accept json
// @Produce json
// @Param position_id path string true "position_id"
// @Param position body models.CreatePositionModel true "position"
// @Success 200 {object} models.ResponseModel{data=models.Status} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) UpdatePosition(c *gin.Context) {
	var status models.Status
	var position models.CreatePositionModel

	var inArray = position.PositionAttributes
    for i:=0; i<len(inArray);i++{
	fmt.Println(inArray[i])
     }
	position_id := c.Param("position_id")

	if !util.IsValidUUID(position_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid position id", errors.New("position id is not valid"))
		return
	}

	err := c.BindJSON(&position)
	if err != nil {
		h.handleErrorResponse(c, http.StatusBadRequest, "error while binding json", err)
		return
	}
	resp, err := h.services.PositionService().Update(
		context.Background(),
		&position_service.GetPositionResponse{
			Id:           position_id,
			Name:         position.Name,
			ProfessionId: position.ProfessionId,
			CompanyId:    position.CompanyId,
		},
	)

	if !handleError(h.log, c, err, "error while getting position") {
		return
	}
fmt.Println(position)
fmt.Println(resp)
	err = ParseToStruct(&status, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", status)
}

// Delete Position godoc
// @ID delete-position
// @Router /v1/position/{position_id} [DELETE]
// @Summary Delete Position
// @Description Delete Position by given ID
// @Tags position
// @Accept json
// @Produce json
// @Param position_id path string true "position_id"
// @Success 200 {object} models.ResponseModel{data=models.Status} "desc"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Response 400 {object} models.ResponseModel{error=string} "Bad Request"
// @Failure 500 {object} models.ResponseModel{error=string} "Server Error"
func (h *handlerV1) DeletePosition(c *gin.Context) {
	var status models.Status
	position_id := c.Param("position_id")

	if !util.IsValidUUID(position_id) {
		h.handleErrorResponse(c, http.StatusBadRequest, "invalid position id", errors.New("position id is not valid"))
		return
	}
	
	resp, err := h.services.PositionService().Delete(
		context.Background(),
		&position_service.PositionId{
			Id: position_id,
		},
	)

	if !handleError(h.log, c, err, "error while getting position") {
		return
	}

	err = ParseToStruct(&status, resp)
	if !handleError(h.log, c, err, "error while parsing to struct") {
		return
	}

	h.handleSuccessResponse(c, http.StatusOK, "ok", status)
}