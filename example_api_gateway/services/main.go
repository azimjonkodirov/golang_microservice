package services

import (
  "fmt"

  "example_api_gateway/config"  
  "example_api_gateway/genproto/position_service"
  "google.golang.org/grpc"
)

type ServiceManager interface {
  ProfessionService() position_service.ProfessionServiceClient  
  CompanyService() position_service.CompanyServiceClient
  AttributeService() position_service.AttributeServiceClient
  PositionService() position_service.PositionServiceClient
}

type grpcClients struct {
  professionService position_service.ProfessionServiceClient  
  companyService    position_service.CompanyServiceClient
  attributeService    position_service.AttributeServiceClient
  positionService    position_service.PositionServiceClient
  
}

func NewGrpcClients(conf *config.Config) (ServiceManager, error) {
  connProfessionService, err := grpc.Dial(
    fmt.Sprintf("%s:%d", conf.PositionServiceHost, conf.PositionServicePort),
    grpc.WithInsecure())
  if err != nil {
    return nil, err
  }

  connCompanyService, err := grpc.Dial(
    fmt.Sprintf("%s:%d", conf.PositionServiceHost, conf.PositionServicePort),
    grpc.WithInsecure())
  if err != nil {
    return nil, err
  }
  connAttributeService, err := grpc.Dial(
    fmt.Sprintf("%s:%d", conf.PositionServiceHost, conf.PositionServicePort),
    grpc.WithInsecure())
  if err != nil {
    return nil, err
  }
  connPositionService, err := grpc.Dial(
    fmt.Sprintf("%s:%d", conf.PositionServiceHost, conf.PositionServicePort),
    grpc.WithInsecure())
  if err != nil {
    return nil, err
  }

  x := &grpcClients{
    professionService: position_service.NewProfessionServiceClient(connProfessionService),
    companyService:    position_service.NewCompanyServiceClient(connCompanyService),
    attributeService:  position_service.NewAttributeServiceClient(connAttributeService),
    positionService:   position_service.NewPositionServiceClient(connPositionService),
  }
  return x, nil
}

func (g *grpcClients) ProfessionService() position_service.ProfessionServiceClient {
  return g.professionService
}

func (g *grpcClients) CompanyService() position_service.CompanyServiceClient {
  return g.companyService
}

func (g *grpcClients) AttributeService() position_service.AttributeServiceClient {
  return g.attributeService
}

func (g *grpcClients) PositionService() position_service.PositionServiceClient {
  return g.positionService
}