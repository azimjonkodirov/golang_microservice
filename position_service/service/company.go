package service

import (
	"context"

	"position_service/genproto/position_service"
	"position_service/pkg/helper"
	"position_service/pkg/logger"
	"position_service/storage"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
)

type companyService struct {
	logger logger.Logger
	position_service.UnimplementedCompanyServiceServer
	storage storage.StorageI
}

func NewCompanyService(log logger.Logger, db *sqlx.DB) *companyService {
	return &companyService{
		logger:  log,
		storage: storage.NewStoragePG(db),
	}
}

func (s *companyService) Create(ctx context.Context, req *position_service.CreateCompany) (*position_service.CompanyId, error) {
	id, err := s.storage.Company().Create(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while creating company ", req, codes.Internal)
	}

	return &position_service.CompanyId{
		Id: id,
	}, nil
}

func (s *companyService) Get(ctx context.Context, req *position_service.CompanyId) (*position_service.Company, error) {
	company, err := s.storage.Company().Get(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting company ", req, codes.Internal)
	}

	return company, nil
}

func (s *companyService) GetAll(ctx context.Context, req *position_service.GetAllCompanyRequest) (*position_service.GetAllCompanyResponse, error) {
	companys, err := s.storage.Company().GetAll(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting all company ", req, codes.Internal)
	}

	return companys, nil
}

func (s *companyService) Update(ctx context.Context, req *position_service.Company) (*position_service.CompanyResult, error) {
	result, err := s.storage.Company().Update(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while updating  company", req, codes.Internal)
	}

	return &position_service.CompanyResult{
		Result: result,
	}, nil
}

func (s *companyService) Delete(ctx context.Context, req *position_service.CompanyId) (*position_service.CompanyResult, error) {
	result, err := s.storage.Company().Delete(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while deleting company ", req, codes.Internal)
	}

	return &position_service.CompanyResult{
		Result: result,
	}, nil
}
