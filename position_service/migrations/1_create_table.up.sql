CREATE TYPE "attribute_types" AS ENUM (
  'datetime',
  'text',
  'number'
);

CREATE TABLE "profession" (
  "id" uuid PRIMARY KEY,
  "name" varchar
);

CREATE TABLE "attribute" (
  "id" uuid PRIMARY KEY,
  "name" varchar,
  "type" attribute_types
);

CREATE TABLE IF NOT EXISTS "position" (
    "id" uuid primary key,
    "name" varchar,
    "profession_id" uuid  references profession(id),
    "company_id" uuid not null
);

CREATE TABLE IF NOT EXISTS "position_attributes" (
    "id" uuid primary key,
    "attribute_id" uuid  references attribute(id),
    "position_id" uuid  references position(id),
    "value" varchar
);

CREATE TABLE "company" (
  "id" uuid PRIMARY KEY,
  "name" varchar
);


