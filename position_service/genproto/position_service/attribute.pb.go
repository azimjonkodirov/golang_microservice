// Code generated by protoc-gen-go. DO NOT EDIT.
// source: attribute.proto

package position_service

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Attribute struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	AttributeType        string   `protobuf:"bytes,3,opt,name=attribute_type,json=attributeType,proto3" json:"attribute_type,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Attribute) Reset()         { *m = Attribute{} }
func (m *Attribute) String() string { return proto.CompactTextString(m) }
func (*Attribute) ProtoMessage()    {}
func (*Attribute) Descriptor() ([]byte, []int) {
	return fileDescriptor_64cfbe2f04478930, []int{0}
}

func (m *Attribute) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Attribute.Unmarshal(m, b)
}
func (m *Attribute) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Attribute.Marshal(b, m, deterministic)
}
func (m *Attribute) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Attribute.Merge(m, src)
}
func (m *Attribute) XXX_Size() int {
	return xxx_messageInfo_Attribute.Size(m)
}
func (m *Attribute) XXX_DiscardUnknown() {
	xxx_messageInfo_Attribute.DiscardUnknown(m)
}

var xxx_messageInfo_Attribute proto.InternalMessageInfo

func (m *Attribute) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Attribute) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Attribute) GetAttributeType() string {
	if m != nil {
		return m.AttributeType
	}
	return ""
}

type AttributeId struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *AttributeId) Reset()         { *m = AttributeId{} }
func (m *AttributeId) String() string { return proto.CompactTextString(m) }
func (*AttributeId) ProtoMessage()    {}
func (*AttributeId) Descriptor() ([]byte, []int) {
	return fileDescriptor_64cfbe2f04478930, []int{1}
}

func (m *AttributeId) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AttributeId.Unmarshal(m, b)
}
func (m *AttributeId) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AttributeId.Marshal(b, m, deterministic)
}
func (m *AttributeId) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AttributeId.Merge(m, src)
}
func (m *AttributeId) XXX_Size() int {
	return xxx_messageInfo_AttributeId.Size(m)
}
func (m *AttributeId) XXX_DiscardUnknown() {
	xxx_messageInfo_AttributeId.DiscardUnknown(m)
}

var xxx_messageInfo_AttributeId proto.InternalMessageInfo

func (m *AttributeId) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

type AttributeResult struct {
	Status               string   `protobuf:"bytes,1,opt,name=status,proto3" json:"status,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *AttributeResult) Reset()         { *m = AttributeResult{} }
func (m *AttributeResult) String() string { return proto.CompactTextString(m) }
func (*AttributeResult) ProtoMessage()    {}
func (*AttributeResult) Descriptor() ([]byte, []int) {
	return fileDescriptor_64cfbe2f04478930, []int{2}
}

func (m *AttributeResult) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AttributeResult.Unmarshal(m, b)
}
func (m *AttributeResult) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AttributeResult.Marshal(b, m, deterministic)
}
func (m *AttributeResult) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AttributeResult.Merge(m, src)
}
func (m *AttributeResult) XXX_Size() int {
	return xxx_messageInfo_AttributeResult.Size(m)
}
func (m *AttributeResult) XXX_DiscardUnknown() {
	xxx_messageInfo_AttributeResult.DiscardUnknown(m)
}

var xxx_messageInfo_AttributeResult proto.InternalMessageInfo

func (m *AttributeResult) GetStatus() string {
	if m != nil {
		return m.Status
	}
	return ""
}

type GetAllAttributeRequest struct {
	Offset               int64    `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit                int64    `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Name                 string   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetAllAttributeRequest) Reset()         { *m = GetAllAttributeRequest{} }
func (m *GetAllAttributeRequest) String() string { return proto.CompactTextString(m) }
func (*GetAllAttributeRequest) ProtoMessage()    {}
func (*GetAllAttributeRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_64cfbe2f04478930, []int{3}
}

func (m *GetAllAttributeRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetAllAttributeRequest.Unmarshal(m, b)
}
func (m *GetAllAttributeRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetAllAttributeRequest.Marshal(b, m, deterministic)
}
func (m *GetAllAttributeRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetAllAttributeRequest.Merge(m, src)
}
func (m *GetAllAttributeRequest) XXX_Size() int {
	return xxx_messageInfo_GetAllAttributeRequest.Size(m)
}
func (m *GetAllAttributeRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetAllAttributeRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetAllAttributeRequest proto.InternalMessageInfo

func (m *GetAllAttributeRequest) GetOffset() int64 {
	if m != nil {
		return m.Offset
	}
	return 0
}

func (m *GetAllAttributeRequest) GetLimit() int64 {
	if m != nil {
		return m.Limit
	}
	return 0
}

func (m *GetAllAttributeRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type GetAllAttributeResponse struct {
	Attributes           []*Attribute `protobuf:"bytes,1,rep,name=attributes,proto3" json:"attributes,omitempty"`
	Count                int32        `protobuf:"varint,2,opt,name=count,proto3" json:"count,omitempty"`
	XXX_NoUnkeyedLiteral struct{}     `json:"-"`
	XXX_unrecognized     []byte       `json:"-"`
	XXX_sizecache        int32        `json:"-"`
}

func (m *GetAllAttributeResponse) Reset()         { *m = GetAllAttributeResponse{} }
func (m *GetAllAttributeResponse) String() string { return proto.CompactTextString(m) }
func (*GetAllAttributeResponse) ProtoMessage()    {}
func (*GetAllAttributeResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_64cfbe2f04478930, []int{4}
}

func (m *GetAllAttributeResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetAllAttributeResponse.Unmarshal(m, b)
}
func (m *GetAllAttributeResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetAllAttributeResponse.Marshal(b, m, deterministic)
}
func (m *GetAllAttributeResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetAllAttributeResponse.Merge(m, src)
}
func (m *GetAllAttributeResponse) XXX_Size() int {
	return xxx_messageInfo_GetAllAttributeResponse.Size(m)
}
func (m *GetAllAttributeResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetAllAttributeResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetAllAttributeResponse proto.InternalMessageInfo

func (m *GetAllAttributeResponse) GetAttributes() []*Attribute {
	if m != nil {
		return m.Attributes
	}
	return nil
}

func (m *GetAllAttributeResponse) GetCount() int32 {
	if m != nil {
		return m.Count
	}
	return 0
}

func init() {
	proto.RegisterType((*Attribute)(nil), "genproto.Attribute")
	proto.RegisterType((*AttributeId)(nil), "genproto.AttributeId")
	proto.RegisterType((*AttributeResult)(nil), "genproto.AttributeResult")
	proto.RegisterType((*GetAllAttributeRequest)(nil), "genproto.GetAllAttributeRequest")
	proto.RegisterType((*GetAllAttributeResponse)(nil), "genproto.GetAllAttributeResponse")
}

func init() { proto.RegisterFile("attribute.proto", fileDescriptor_64cfbe2f04478930) }

var fileDescriptor_64cfbe2f04478930 = []byte{
	// 257 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x64, 0x50, 0xc1, 0x4a, 0x03, 0x31,
	0x10, 0xa5, 0xbb, 0xb6, 0xd8, 0x29, 0xb6, 0x10, 0xa5, 0xae, 0x88, 0x20, 0x01, 0xa1, 0x5e, 0x56,
	0xb0, 0x5f, 0x50, 0x2f, 0xe2, 0x35, 0x88, 0x87, 0x5e, 0xca, 0xb6, 0x3b, 0x95, 0xc0, 0x36, 0x89,
	0x3b, 0x13, 0xa1, 0x7f, 0x2f, 0xcd, 0xee, 0xa6, 0xc5, 0xde, 0xf2, 0xde, 0x9b, 0xf7, 0x66, 0x5e,
	0x60, 0x52, 0x30, 0xd7, 0x7a, 0xed, 0x19, 0x73, 0x57, 0x5b, 0xb6, 0xe2, 0xf2, 0x1b, 0x4d, 0x78,
	0xc9, 0x2f, 0x18, 0x2e, 0x3a, 0x51, 0x8c, 0x21, 0xd1, 0x65, 0xd6, 0x7b, 0xec, 0xcd, 0x86, 0x2a,
	0xd1, 0xa5, 0x10, 0x70, 0x61, 0x8a, 0x1d, 0x66, 0x49, 0x60, 0xc2, 0x5b, 0x3c, 0xc1, 0x38, 0xa6,
	0xad, 0x78, 0xef, 0x30, 0x4b, 0x83, 0x7a, 0x15, 0xd9, 0xcf, 0xbd, 0x43, 0xf9, 0x00, 0xa3, 0x98,
	0xfb, 0x51, 0xfe, 0x4f, 0x96, 0xcf, 0x30, 0x89, 0xb2, 0x42, 0xf2, 0x15, 0x8b, 0x29, 0x0c, 0x88,
	0x0b, 0xf6, 0xd4, 0x8e, 0xb5, 0x48, 0x2e, 0x61, 0xfa, 0x8e, 0xbc, 0xa8, 0xaa, 0x13, 0xc3, 0x8f,
	0x47, 0x0a, 0x0e, 0xbb, 0xdd, 0x12, 0x72, 0x70, 0xa4, 0xaa, 0x45, 0xe2, 0x06, 0xfa, 0x95, 0xde,
	0x69, 0x0e, 0x77, 0xa7, 0xaa, 0x01, 0xb1, 0x4c, 0x7a, 0x2c, 0x23, 0x4b, 0xb8, 0x3d, 0xcb, 0x26,
	0x67, 0x0d, 0xa1, 0x98, 0x03, 0xc4, 0x46, 0x87, 0x93, 0xd2, 0xd9, 0xe8, 0xf5, 0x3a, 0xef, 0xfe,
	0x2d, 0x3f, 0x1a, 0x4e, 0xc6, 0x0e, 0x9b, 0x37, 0xd6, 0x9b, 0x66, 0x73, 0x5f, 0x35, 0xe0, 0xed,
	0x7e, 0x79, 0xd7, 0xf9, 0x5e, 0x9c, 0x25, 0xcd, 0xda, 0x9a, 0x15, 0x61, 0xfd, 0xab, 0x37, 0xb8,
	0x1e, 0x04, 0x7e, 0xfe, 0x17, 0x00, 0x00, 0xff, 0xff, 0x67, 0x2e, 0x37, 0x21, 0xa4, 0x01, 0x00,
	0x00,
}
