// Code generated by protoc-gen-go. DO NOT EDIT.
// source: company.proto

package position_service

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Company struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Company) Reset()         { *m = Company{} }
func (m *Company) String() string { return proto.CompactTextString(m) }
func (*Company) ProtoMessage()    {}
func (*Company) Descriptor() ([]byte, []int) {
	return fileDescriptor_ade57ca5b8f3903f, []int{0}
}

func (m *Company) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Company.Unmarshal(m, b)
}
func (m *Company) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Company.Marshal(b, m, deterministic)
}
func (m *Company) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Company.Merge(m, src)
}
func (m *Company) XXX_Size() int {
	return xxx_messageInfo_Company.Size(m)
}
func (m *Company) XXX_DiscardUnknown() {
	xxx_messageInfo_Company.DiscardUnknown(m)
}

var xxx_messageInfo_Company proto.InternalMessageInfo

func (m *Company) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Company) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type CreateCompany struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateCompany) Reset()         { *m = CreateCompany{} }
func (m *CreateCompany) String() string { return proto.CompactTextString(m) }
func (*CreateCompany) ProtoMessage()    {}
func (*CreateCompany) Descriptor() ([]byte, []int) {
	return fileDescriptor_ade57ca5b8f3903f, []int{1}
}

func (m *CreateCompany) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateCompany.Unmarshal(m, b)
}
func (m *CreateCompany) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateCompany.Marshal(b, m, deterministic)
}
func (m *CreateCompany) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateCompany.Merge(m, src)
}
func (m *CreateCompany) XXX_Size() int {
	return xxx_messageInfo_CreateCompany.Size(m)
}
func (m *CreateCompany) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateCompany.DiscardUnknown(m)
}

var xxx_messageInfo_CreateCompany proto.InternalMessageInfo

func (m *CreateCompany) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type CompanyId struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CompanyId) Reset()         { *m = CompanyId{} }
func (m *CompanyId) String() string { return proto.CompactTextString(m) }
func (*CompanyId) ProtoMessage()    {}
func (*CompanyId) Descriptor() ([]byte, []int) {
	return fileDescriptor_ade57ca5b8f3903f, []int{2}
}

func (m *CompanyId) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CompanyId.Unmarshal(m, b)
}
func (m *CompanyId) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CompanyId.Marshal(b, m, deterministic)
}
func (m *CompanyId) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CompanyId.Merge(m, src)
}
func (m *CompanyId) XXX_Size() int {
	return xxx_messageInfo_CompanyId.Size(m)
}
func (m *CompanyId) XXX_DiscardUnknown() {
	xxx_messageInfo_CompanyId.DiscardUnknown(m)
}

var xxx_messageInfo_CompanyId proto.InternalMessageInfo

func (m *CompanyId) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

type GetAllCompanyRequest struct {
	Limit                uint32   `protobuf:"varint,1,opt,name=limit,proto3" json:"limit,omitempty"`
	Offset               uint32   `protobuf:"varint,2,opt,name=offset,proto3" json:"offset,omitempty"`
	Name                 string   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetAllCompanyRequest) Reset()         { *m = GetAllCompanyRequest{} }
func (m *GetAllCompanyRequest) String() string { return proto.CompactTextString(m) }
func (*GetAllCompanyRequest) ProtoMessage()    {}
func (*GetAllCompanyRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_ade57ca5b8f3903f, []int{3}
}

func (m *GetAllCompanyRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetAllCompanyRequest.Unmarshal(m, b)
}
func (m *GetAllCompanyRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetAllCompanyRequest.Marshal(b, m, deterministic)
}
func (m *GetAllCompanyRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetAllCompanyRequest.Merge(m, src)
}
func (m *GetAllCompanyRequest) XXX_Size() int {
	return xxx_messageInfo_GetAllCompanyRequest.Size(m)
}
func (m *GetAllCompanyRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetAllCompanyRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetAllCompanyRequest proto.InternalMessageInfo

func (m *GetAllCompanyRequest) GetLimit() uint32 {
	if m != nil {
		return m.Limit
	}
	return 0
}

func (m *GetAllCompanyRequest) GetOffset() uint32 {
	if m != nil {
		return m.Offset
	}
	return 0
}

func (m *GetAllCompanyRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type GetAllCompanyResponse struct {
	Companies            []*Company `protobuf:"bytes,1,rep,name=companies,proto3" json:"companies,omitempty"`
	Count                uint32     `protobuf:"varint,2,opt,name=count,proto3" json:"count,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *GetAllCompanyResponse) Reset()         { *m = GetAllCompanyResponse{} }
func (m *GetAllCompanyResponse) String() string { return proto.CompactTextString(m) }
func (*GetAllCompanyResponse) ProtoMessage()    {}
func (*GetAllCompanyResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_ade57ca5b8f3903f, []int{4}
}

func (m *GetAllCompanyResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetAllCompanyResponse.Unmarshal(m, b)
}
func (m *GetAllCompanyResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetAllCompanyResponse.Marshal(b, m, deterministic)
}
func (m *GetAllCompanyResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetAllCompanyResponse.Merge(m, src)
}
func (m *GetAllCompanyResponse) XXX_Size() int {
	return xxx_messageInfo_GetAllCompanyResponse.Size(m)
}
func (m *GetAllCompanyResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetAllCompanyResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetAllCompanyResponse proto.InternalMessageInfo

func (m *GetAllCompanyResponse) GetCompanies() []*Company {
	if m != nil {
		return m.Companies
	}
	return nil
}

func (m *GetAllCompanyResponse) GetCount() uint32 {
	if m != nil {
		return m.Count
	}
	return 0
}

type CompanyResult struct {
	Result               string   `protobuf:"bytes,1,opt,name=result,proto3" json:"result,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CompanyResult) Reset()         { *m = CompanyResult{} }
func (m *CompanyResult) String() string { return proto.CompactTextString(m) }
func (*CompanyResult) ProtoMessage()    {}
func (*CompanyResult) Descriptor() ([]byte, []int) {
	return fileDescriptor_ade57ca5b8f3903f, []int{5}
}

func (m *CompanyResult) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CompanyResult.Unmarshal(m, b)
}
func (m *CompanyResult) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CompanyResult.Marshal(b, m, deterministic)
}
func (m *CompanyResult) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CompanyResult.Merge(m, src)
}
func (m *CompanyResult) XXX_Size() int {
	return xxx_messageInfo_CompanyResult.Size(m)
}
func (m *CompanyResult) XXX_DiscardUnknown() {
	xxx_messageInfo_CompanyResult.DiscardUnknown(m)
}

var xxx_messageInfo_CompanyResult proto.InternalMessageInfo

func (m *CompanyResult) GetResult() string {
	if m != nil {
		return m.Result
	}
	return ""
}

func init() {
	proto.RegisterType((*Company)(nil), "genproto.Company")
	proto.RegisterType((*CreateCompany)(nil), "genproto.CreateCompany")
	proto.RegisterType((*CompanyId)(nil), "genproto.CompanyId")
	proto.RegisterType((*GetAllCompanyRequest)(nil), "genproto.GetAllCompanyRequest")
	proto.RegisterType((*GetAllCompanyResponse)(nil), "genproto.GetAllCompanyResponse")
	proto.RegisterType((*CompanyResult)(nil), "genproto.CompanyResult")
}

func init() { proto.RegisterFile("company.proto", fileDescriptor_ade57ca5b8f3903f) }

var fileDescriptor_ade57ca5b8f3903f = []byte{
	// 250 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x5c, 0x90, 0x41, 0x4b, 0xc3, 0x40,
	0x10, 0x85, 0x49, 0xaa, 0xd5, 0x8c, 0xac, 0xe0, 0x52, 0x25, 0xd2, 0x4b, 0x89, 0x07, 0x7b, 0x31,
	0x05, 0xfd, 0x05, 0xda, 0x83, 0x78, 0xcd, 0x49, 0x3c, 0x28, 0x31, 0x99, 0xca, 0x40, 0xb2, 0x13,
	0xb3, 0x1b, 0xc1, 0x7f, 0x2f, 0x4e, 0x76, 0xdb, 0xd2, 0xdb, 0xbc, 0x99, 0x6f, 0xdf, 0xbc, 0x59,
	0x50, 0x15, 0xb7, 0x5d, 0x69, 0x7e, 0xf3, 0xae, 0x67, 0xc7, 0xfa, 0xf4, 0x0b, 0x8d, 0x54, 0xd9,
	0x1d, 0x9c, 0xac, 0xc7, 0x91, 0x3e, 0x87, 0x98, 0xea, 0x34, 0x5a, 0x44, 0xcb, 0xa4, 0x88, 0xa9,
	0xd6, 0x1a, 0x8e, 0x4c, 0xd9, 0x62, 0x1a, 0x4b, 0x47, 0xea, 0xec, 0x06, 0xd4, 0xba, 0xc7, 0xd2,
	0x61, 0x78, 0x14, 0xa0, 0x68, 0x0f, 0x9a, 0x43, 0xe2, 0xc7, 0x2f, 0xf5, 0xa1, 0x6b, 0xf6, 0x0a,
	0xb3, 0x67, 0x74, 0x8f, 0x4d, 0xe3, 0x91, 0x02, 0xbf, 0x07, 0xb4, 0x4e, 0xcf, 0xe0, 0xb8, 0xa1,
	0x96, 0x9c, 0xa0, 0xaa, 0x18, 0x85, 0xbe, 0x82, 0x29, 0x6f, 0x36, 0x16, 0x9d, 0xa4, 0x50, 0x85,
	0x57, 0xdb, 0xb5, 0x93, 0xbd, 0xb5, 0xef, 0x70, 0x79, 0xe0, 0x6c, 0x3b, 0x36, 0x16, 0xf5, 0x0a,
	0x92, 0xf1, 0x7c, 0x42, 0x9b, 0x46, 0x8b, 0xc9, 0xf2, 0xec, 0xfe, 0x22, 0x0f, 0x3f, 0x90, 0x07,
	0x7a, 0xc7, 0xfc, 0x67, 0xa9, 0x78, 0x30, 0x61, 0xe9, 0x28, 0xb2, 0x5b, 0x50, 0x3b, 0xe7, 0xa1,
	0x91, 0x70, 0xbd, 0x54, 0xfe, 0x3c, 0xaf, 0x9e, 0xe6, 0x6f, 0xd7, 0xc1, 0x7d, 0xd5, 0xb1, 0x25,
	0x47, 0x6c, 0x3e, 0x2c, 0xf6, 0x3f, 0x54, 0xe1, 0xe7, 0x54, 0xfa, 0x0f, 0x7f, 0x01, 0x00, 0x00,
	0xff, 0xff, 0x96, 0xd4, 0x4d, 0x6f, 0x92, 0x01, 0x00, 0x00,
}
