package storage

import (
	"position_service/storage/postgres"
	"position_service/storage/repo"

	"github.com/jmoiron/sqlx"
)

type StorageI interface {
	Profession() repo.ProfessionRepoI
	Attribute() repo.AttributeRepoI
	Company() repo.CompanyRepoI
	Position() repo.PositionRepoI
}

type storagePG struct {
	profession repo.ProfessionRepoI
	attribute  repo.AttributeRepoI
	company    repo.CompanyRepoI
	position   repo.PositionRepoI
}

func NewStoragePG(db *sqlx.DB) StorageI {
	return &storagePG{
		profession: postgres.NewProfessionRepo(db),
		attribute:  postgres.NewAttributeRepo(db),
		company:    postgres.NewCompanyRepo(db),
		position:   postgres.NewPositionRepo(db),
	}
}

func (s *storagePG) Profession() repo.ProfessionRepoI {
	return s.profession
}
func (s *storagePG) Attribute() repo.AttributeRepoI {
	return s.attribute
}
func (s *storagePG) Company() repo.CompanyRepoI {
	return s.company
}
func (s *storagePG) Position() repo.PositionRepoI {
	return s.position
}