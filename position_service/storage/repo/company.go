package repo

import "position_service/genproto/position_service"

type CompanyRepoI interface {
	Create(req *position_service.CreateCompany) (string, error)
	Get(id string) (*position_service.Company, error)
	GetAll(req *position_service.GetAllCompanyRequest) (*position_service.GetAllCompanyResponse, error)
	Update(req *position_service.Company) (string, error)
	Delete(id string) (string, error)
}