module position_service

go 1.17

require github.com/jmoiron/sqlx v1.3.4

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)

require (
	github.com/golang/protobuf v1.5.0
	github.com/google/uuid v1.3.0
	github.com/lib/pq v1.10.4
	github.com/spf13/cast v1.4.1
	go.uber.org/zap v1.19.1
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	google.golang.org/grpc v1.43.0
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
